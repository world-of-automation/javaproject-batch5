package secondpackage;

public class LaptopExecution {

    public static void main(String[] args) {

        // static method, in order to go to different class, you have to call via class name
        Laptop.printLaptopModel();

        // non static --> same approach as in the same class
        Laptop object = new Laptop();

        object.printLaptopMake();

        System.out.println(object.year);

        //System.out.println(object.ram);
    }


}
