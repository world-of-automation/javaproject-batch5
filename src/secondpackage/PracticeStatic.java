package secondpackage;

public class PracticeStatic {

    // create 2 static variables and 1 non static variables (with value)

    private static final String name = "Zan";
    private static final int age = 27;
    private static final PracticeStatic object = new PracticeStatic();
    private final int zip = 11374;

    // create 1 static method and 1 non static method

    private static void staticM1() {
        // print the static variables into static method
        System.out.println(name + age);
        // print the non static variable into static method
        System.out.println(object.zip);
    }

    public static void main(String[] args) {
        staticM1();
        object.nonStaticM1();
    }

    // finally call both the methods in the main method

    private void nonStaticM1() {
        // print the static variables into non static method
        System.out.println(name + age);

        // print the non static variables into non static method
        System.out.println(zip);

    }

}
