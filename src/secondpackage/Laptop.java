package secondpackage;

public class Laptop {

    private static final String model = "Macbook Pro";
    public static String ram = "16 GB";
    private final String make = "Apple";
    public int year = 2021;

    public static void printLaptopModel() {
        System.out.println(model);
    }

    public void printLaptopMake() {
        System.out.println(make);
    }
}
