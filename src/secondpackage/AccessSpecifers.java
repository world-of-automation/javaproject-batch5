package secondpackage;

public class AccessSpecifers {

    //  static variable
    private static final int age = 25;

    // public --> can be accessed by any other classes
    // private  --> only in the same class
    // protected
    // default


    public AccessSpecifers() {//default constructor

    }

    public static void main(String[] args) {
        test1();
        test2();

    }

    // 2 method names can't be same
    private static void test1() {

    }

    protected static void test2() {

    }

    static void test3() {

    }

}
