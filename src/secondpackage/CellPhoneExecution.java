package secondpackage;

public class CellPhoneExecution {

    public static void main(String[] args) {

        CellPhone cellPhone = new CellPhone("Samsung", "S22", 2022);
        cellPhone.printMakeOfPhone();
        cellPhone.printModelOfPhone();
        //cellPhone.printYearOfPhone();

        CellPhone cellPhone2 = new CellPhone("Apple", "iphone12", 2021);
        cellPhone2.printMakeOfPhone();
        cellPhone2.printModelOfPhone();
//		cellPhone2.printYearOfPhone();


    }

}
