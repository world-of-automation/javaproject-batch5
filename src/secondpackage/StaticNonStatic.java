package secondpackage;

public class StaticNonStatic {

    //  static variable
    private static final int age = 25;
    // non static variable
    private final String name = "MD";

    public StaticNonStatic() {//default constructor
        // constructor name is always same as class name
    }

    public static void main(String[] args) {
        test1();
        System.out.println(age); // static --> jvm doesn't need the constructor

        // using object
        // className     refferenceVariable/objectName = new constructor();
        StaticNonStatic objectName = new StaticNonStatic();

        objectName.test2();
        System.out.println(objectName.name); // non-static --> jvm does need the constructor


    }

    // static can go inside another static method or non static
    // non static can't go inside another static method bcz it needs constructor
    // non static can go inside another non-static


    // 2 method names can't be same
    private static void test1() {
        System.out.println(age);

        // className     refferenceVariable/objectName = new constructor();
        StaticNonStatic objectName = new StaticNonStatic();

        System.out.println(objectName.name);
    }

    protected void test2() {
        System.out.println(age);
        System.out.println(name);
    }

}
