package secondpackage;

public class CellPhone {

    private String make;
    private String model;
    private int year;


    // default constructor
    public CellPhone() {

    }

    //parameterized constructor
    public CellPhone(String makeOfPhone, String modelOfPhone, int yearOfPhone) {
        make = makeOfPhone;
        model = modelOfPhone;
        year = yearOfPhone;
    }

    //parameterized constructor
    public CellPhone(String makeOfPhone, String modelOfPhone) {
        make = makeOfPhone;
        model = modelOfPhone;
    }

    public void printMakeOfPhone() {
        System.out.println(make);
    }

    public void printModelOfPhone() {
        System.out.println(model);
    }

    public void printYearOfPhone() {
        System.out.println(year);
    }


}
