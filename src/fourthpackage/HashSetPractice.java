package fourthpackage;

import java.util.HashSet;
import java.util.Iterator;

public class HashSetPractice {

    public static void main(String[] args) {
        HashSet<String> setOfData = new HashSet<>();
        setOfData.add("NY");
        setOfData.add("NJ");
        setOfData.add("VA");
        setOfData.add("DC");
        setOfData.add("VA");


        System.out.println(setOfData.size());
        System.out.println(setOfData);

       /* System.out.println(setOfData.get(1));

        for(int i=0;i<setOfData.size();i++){
            System.out.println(setOfData.get(1));

        }*/

        for (String data : setOfData) {
            System.out.println(data);
        }


        Iterator it = setOfData.iterator();

        while (it.hasNext()) {
            System.out.println(it.next());
        }

        HashSet<String> setOfData2 = (HashSet<String>) setOfData.clone();
        System.out.println(setOfData2);


        // Homework : create a arraylist of hashmap and show usage (add,print,remove,clone etc)
        // Google and find a way to print all the value of a set using a loop
    }
}
