package fourthpackage;

import java.util.ArrayList;
import java.util.HashMap;

public class HashMapPractice {

    public static void main(String[] args) {
        // key value pair

        HashMap<String, String> personalInfo = new HashMap();
        personalInfo.put("name", "Zann");
        personalInfo.put("location", "rego park");
        personalInfo.put("zipcode", "45464");
        personalInfo.put("job", "SDET");


        System.out.println(personalInfo.get("job"));

        System.out.println(personalInfo.size());


        // create a map of america with state names and their acronyms


        HashMap<String, String> america = new HashMap();

        america.put("New York", "NY");
        america.put("New Jersey", "NJ");


        america.remove("New Jersey");
        System.out.println(america);


        HashMap<String, String> map1 = new HashMap();
        map1.put("a", "Zann");
        map1.put("b", "Md");
        map1.put("c", "Sub");

        map1.put("b", "Jawad");
        map1.put(null, "Opu");
        map1.put(null, "Samiyan");

        map1.put("d", null);
        map1.put("e", null);


        System.out.println(map1);


        // entry set --> later


        // create a map of String as key and arraylist of string as value
        // <String,Arraylist<String>>

        ArrayList<String> listOfCountries = new ArrayList<>();
        listOfCountries.add("USA");
        listOfCountries.add("Canada");
        listOfCountries.add("Spain");

        ArrayList<String> listOfOceans = new ArrayList<>();
        listOfOceans.add("Atlantic");
        listOfOceans.add("Pacific");
        listOfOceans.add("Indian");
        listOfOceans.add("Arctic");

        HashMap<String, ArrayList<String>> mapOfTheWorld = new HashMap<>();

        mapOfTheWorld.put("countries", listOfCountries);
        mapOfTheWorld.put("oceans", listOfOceans);


        ArrayList<String> oo = mapOfTheWorld.get("countries");
        System.out.println(oo.get(0));

        System.out.println(mapOfTheWorld.get("countries").get(0));


    }
}
