package fourthpackage;

import java.util.ArrayList;

public class ArrayListPractice {

    public static void main(String[] args) {
        ArrayList<String> listOfNames = new ArrayList<String>();
        listOfNames.add("MD");
        listOfNames.add("Zann");
        listOfNames.add("Sub");
        listOfNames.add("Jawad");
        listOfNames.add("Samiyan");
        listOfNames.add(null);
        listOfNames.add(null);


        System.out.println(listOfNames);

        System.out.println(listOfNames.size());


        for (int i = 0; i < listOfNames.size(); i++) {
            System.out.println(listOfNames.get(i));
        }


        ArrayList<Object> objectArrayList = new ArrayList<>();
        ArrayList objectArrayList2 = new ArrayList();
        objectArrayList.add(true);
        objectArrayList.add(1123);

        ArrayList<Double> numbers = new ArrayList<>();


        listOfNames.add("OPU");
        System.out.println(listOfNames.size());


        listOfNames.remove(3);
        System.out.println(listOfNames);


        listOfNames.remove("OPU");
        System.out.println(listOfNames);


        System.out.println(listOfNames.isEmpty());


        listOfNames.clear();
        System.out.println(listOfNames);

        System.out.println(listOfNames.isEmpty());


    }
}
