package sixthpackage;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class FileReaderUtils {

    public static void main(String[] args) throws IOException {
        String valueOfPropertiesFile = getPropertyOfFile("src/sixthpackage/Config2.properties", "username");
        System.out.println(valueOfPropertiesFile);

        String dataFromTextFile = getTextFile("src/sixthpackage/Information.txt");
        System.out.println(dataFromTextFile);
    }

    public static String getPropertyOfFile(String filePath, String key) throws IOException {
        Properties properties = new Properties();
        // FileInputStream fs = new FileInputStream(filePath);
        // properties.load(fs);
        properties.load(new FileInputStream(filePath));
        // String value = properties.getProperty(key);
        // return value;
        return properties.getProperty(key);
    }


    public static String getTextFile(String filePath) throws IOException {
        StringBuilder finalText = new StringBuilder();
        BufferedReader br = new BufferedReader(new FileReader(filePath));
        String tempContainer = "";

        while ((tempContainer = br.readLine()) != null) {
            finalText.append(tempContainer);
        }

        // find out to print same as the text file (with next line)
        return finalText.toString();
    }


}
