package sixthpackage;

public class Laptop {

    // singleton class

    // lazy initialization of object
    private static Laptop laptop;

    // static return of the object of the class
    public static Laptop getLaptop() {

        if (laptop == null) {
            System.out.println("Object is getting created");
            laptop = new Laptop();
        }

        return laptop;
    }


    public void printLaptopDetails() {
        System.out.println("Apple");
    }

}
