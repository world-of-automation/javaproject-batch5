package sixthpackage;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ScannerPractice {

    public static void main(String[] args) {

          /* int i;
        for(i=0;i<10;i++){
            System.out.println(i);
        }
        System.out.println(i);*/

        Scanner scanner = new Scanner(System.in);
        System.out.println("please insert your age : ");

        int age = 0;
        while (age == 0) {
            try {
                // try to do this piece
                age = scanner.nextInt();
            } catch (InputMismatchException e) {
                // if there's an exception, catch that and perform below
                //e.printStackTrace();
                System.out.println("please insert your age in number : ");
                scanner.nextLine();
            } finally {
                // finally always gets executed
            }
        }

        System.out.println("my age is : " + age);
    }
}
