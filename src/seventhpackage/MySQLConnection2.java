package seventhpackage;

import sixthpackage.FileReaderUtils;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class MySQLConnection2 {

    private static Connection connection = null;
    private static Statement statement = null;

    public static ResultSet connectToMysql(String query) throws SQLException, IOException {
        String username = FileReaderUtils.getPropertyOfFile("src/seventhpackage/config.properties", "mysqlUsername");
        String password = FileReaderUtils.getPropertyOfFile("src/seventhpackage/config.properties", "mysqlPassword");
        String url = "jdbc:mysql://localhost:3306/classicmodels";
        connection = DriverManager.getConnection(url, username, password);
        statement = connection.createStatement();
        return statement.executeQuery(query);
    }

    public static void clearConnections() throws SQLException {
        statement.close();
        connection.close();
    }

    public static ArrayList<String> getValue(ResultSet rs, String columnName) throws SQLException {
        ArrayList<String> list = new ArrayList<>();
        while (rs.next()) {
            list.add(rs.getString(columnName));
        }
        rs.close();
        return list;
    }


    public static HashMap<String, ArrayList<String>> getValues(ResultSet rs, ArrayList<String> columnNames) throws SQLException {
        HashMap<String, ArrayList<String>> map = new HashMap<>();

        for (String columnName : columnNames) {
            ArrayList<String> list = new ArrayList<>();
            map.put(columnName, list);
        }

        while (rs.next()) {
            for (String columnName : columnNames) {
                map.get(columnName).add(rs.getString(columnName));
            }
        }

        rs.close();
        return map;
    }

}
