package seventhpackage;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class Execution {

    public static void main(String[] args) throws SQLException, IOException {
        ResultSet tableOfCustomers = MySQLConnection2.connectToMysql("select * from customers");
        ArrayList<String> dataFromDB = MySQLConnection2.getValue(tableOfCustomers, "customerName");
        System.out.println(dataFromDB.size());


        ResultSet rsOfEmployees = MySQLConnection2.connectToMysql("select * from employees");
        ArrayList<String> columnNames = new ArrayList<>();
        columnNames.add("email");
        columnNames.add("extension");
        columnNames.add("firstName");

        HashMap<String, ArrayList<String>> alldataFroTable = MySQLConnection2.getValues(rsOfEmployees, columnNames);
        System.out.println(alldataFroTable);
        System.out.println(alldataFroTable.get("email"));

        MySQLConnection2.clearConnections();
    }
}
