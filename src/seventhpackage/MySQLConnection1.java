package seventhpackage;

import sixthpackage.FileReaderUtils;

import java.io.IOException;
import java.sql.*;

public class MySQLConnection1 {


    public static void main(String[] args) throws SQLException, IOException {
        String username = FileReaderUtils.getPropertyOfFile("src/seventhpackage/config.properties", "mysqlUsername");
        String password = FileReaderUtils.getPropertyOfFile("src/seventhpackage/config.properties", "mysqlPassword");

        // drivername :// host name : port number/dbName
        String url = "jdbc:mysql://localhost:3306/classicmodels";

        Connection connection = DriverManager.getConnection(url, username, password);
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery("select * from customers");

        while (rs.next()) {
            System.out.println(rs.getString("phone"));
        }


    }

}
