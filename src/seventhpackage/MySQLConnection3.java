package seventhpackage;

import sixthpackage.FileReaderUtils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class MySQLConnection3 {


    public static void main(String[] args) throws IOException {
        String username = FileReaderUtils.getPropertyOfFile("src/seventhpackage/config.properties", "mysqlUsername");
        String password = FileReaderUtils.getPropertyOfFile("src/seventhpackage/config.properties", "mysqlPassword");

        String url = "jdbc:mysql://localhost:3306/classicmodels";
        ResultSet rs = null;
        Statement statement = null;
        Connection connection = null;

        try {
            connection = DriverManager.getConnection(url, username, password);
            statement = connection.createStatement();
            rs = statement.executeQuery("select * from customers");

            while (rs.next()) {
                System.out.println(rs.getString("phone"));
            }
        } catch (Exception ee) {
            ee.printStackTrace();
        } finally {
            System.out.println("finally getting executed ");
            try {
                rs.close();
                statement.close();
                connection.close();
            } catch (Exception ignored) {

            }
        }


    }

}
