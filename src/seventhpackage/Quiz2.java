package seventhpackage;

import java.util.ArrayList;
import java.util.Scanner;

public class Quiz2 {


    //Built an application for the electric voting system.
    //have 2 candidates predetermined for the vote
    //the application should be able to take x number of peoples names and whom they want to vote for as an input
    //at the end of the application, it should return who is the winner of the vote and how many votes he got


    public static void main(String[] args) {
        String candidate1 = "Trump";
        String candidate2 = "Biden";


        ArrayList<String> votersForTrump = new ArrayList<>();
        ArrayList<String> votersForBiden = new ArrayList<>();

        Scanner scanner = new Scanner(System.in);
        System.out.println("please insert the number of voters : ");
        int totalVoters = scanner.nextInt();
        System.out.println("total voters number is : " + totalVoters);
        scanner.nextLine();

        for (int i = 0; i < totalVoters; i++) {
            System.out.println("please insert the name of voter : ");
            String nameOfVoters = scanner.nextLine();

            System.out.println("please insert to whom " + nameOfVoters + " wants to vote :");
            String answerFromVoter = scanner.nextLine();

            if (answerFromVoter.equalsIgnoreCase(candidate1)) {
                votersForTrump.add(answerFromVoter);

            } else if (answerFromVoter.equalsIgnoreCase(candidate2)) {
                votersForBiden.add(answerFromVoter);
            } else {
                System.out.println("please insert correct name of voter");
            }
        }


        System.out.println("total votes of " + candidate1 + " is : " + votersForTrump.size());
        System.out.println("total votes of " + candidate2 + " is : " + votersForBiden.size());


        System.out.print("******SO the winner is : ");


    }
}
