package seventhpackage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class SoccerTeam {


    public static void main(String[] args) {

        /*In a soccer team  total players are 23
        for the next match find out the total 11 + 7 (SUB) players by  randomizing
        then from the substitutes, sub in x number of players with the beginning 11.
        but you can't sub 3 fixed players.
        At the end print who are the players played full 90 minutes and who got subbed out*/


        List<String> totalPlayers = Arrays.asList("Ronaldo", "Messi", "Neymar", "Mbappe", "Zidane", "Kaka",
                "Figo", "Ronaldinho", "Christiano ROnaldo", "Suarez", "Ramos", "Zlatan", "Pepe", "VVD", "Ahyero", "Salah", "Cassilas", "Md", "Sub"
                , "Jawad", "Pele", "Maradona", "Balettoli");

        System.out.println("Total player number is : " + totalPlayers.size());


        List<String> teamPlayerForNextMatch = new ArrayList<>();

        Random random = new Random();

        while (teamPlayerForNextMatch.size() < 18) {
            int randomNumber = random.nextInt(totalPlayers.size() - 1);
            String player = totalPlayers.get(randomNumber);
            if (!teamPlayerForNextMatch.contains(player)) {
                teamPlayerForNextMatch.add(player);
            }
        }

        System.out.println(teamPlayerForNextMatch.size() + " unique players for the next match are : ");
        System.out.println(teamPlayerForNextMatch);


        List<String> startingPlayerForNextMatch = new ArrayList<>();

        while (startingPlayerForNextMatch.size() < 11) {
            int randomNumber = random.nextInt(teamPlayerForNextMatch.size() - 1);
            String player = teamPlayerForNextMatch.get(randomNumber);
            if (!startingPlayerForNextMatch.contains(player)) {
                startingPlayerForNextMatch.add(player);
            }
        }

        System.out.println(startingPlayerForNextMatch.size() + " starting players for the next match are : ");
        System.out.println(startingPlayerForNextMatch);


        ArrayList<String> substitutesPlayers = new ArrayList(teamPlayerForNextMatch);


        System.out.println("cloned Substitutes player numbers : " + substitutesPlayers.size());
        for (String fPlayer : startingPlayerForNextMatch) {
            substitutesPlayers.remove(fPlayer);
        }

        System.out.println("Substitutes player numbers : " + substitutesPlayers.size());
        System.out.println("Substitutes player are : " + substitutesPlayers);


        ArrayList<String> finalMinutePlayers = new ArrayList<>(startingPlayerForNextMatch);

        for (int i = 0; i < 4; i++) {
            finalMinutePlayers.remove(random.nextInt(finalMinutePlayers.size() - 1));
        }

        System.out.println("After removal : " + finalMinutePlayers);

        for (int i = 0; i < 4; i++) {
            int randomCount = random.nextInt(substitutesPlayers.size() - 1);
            String subbedPlayer = substitutesPlayers.get(randomCount);
            finalMinutePlayers.add(subbedPlayer);
        }


        System.out.println("players who played till the last whistle are : " + finalMinutePlayers);


    }
}
