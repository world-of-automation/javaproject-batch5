package fifthpackage;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HomeWork {


    public static void shuffleList(List<String> songsList) {
        // write the algorithm below to shuffle the song list (and print it)
        // you aren't allowed to use java collections library
        Random random = new Random();

        int totalSong = songsList.size();

        for (int i = 0; i < totalSong; i++) {
            int randomNumber = random.nextInt(totalSong);

            System.out.println(songsList.get(randomNumber));
        }
    }


    public static void main(String[] args) {
        List<String> songs = new ArrayList<>();
        songs.add("sultans of swing");
        songs.add("brothers in arm");
        songs.add("tears in heaven");
        songs.add("limelight");
        songs.add("layla");
        shuffleList(songs);

        String data = "java";
        reverseStringExample(data);
    }

    public static void reverseStringExample(String data) {
        // write the algorithm to reverse this string

        System.out.println("**************");
        System.out.println(data);

        char[] dataArray = data.toCharArray();

        // String finalText = "";


        StringBuilder finalText = new StringBuilder();

        for (int i = dataArray.length - 1; i >= 0; i--) {
            String textByIndex = String.valueOf(dataArray[i]);
            // finalText = finalText + textByIndex;

            finalText.append(textByIndex);

        }

        System.out.println(finalText);
    }

}

