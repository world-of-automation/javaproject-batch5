package fifthpackage.oop;

public abstract class Car extends Car2 {

    // it will have regular methods & methods without body
    // method without bodies you have to specify the keyword abstract


    // can have constructor but still you can't create an object
    // abstract methods and the abstract class name must be mentioned with the keyword abstract
    // we can extend abstract class but to only 1
    // we can implement multiple interfaces to abstract class


    public void wheels() {
        System.out.println("Car has wheels");
    }


    public void key() {
        System.out.println("Car has key");
    }


    public abstract void fuel();

}
