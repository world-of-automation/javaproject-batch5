package fifthpackage.oop;

public class Execution {

    public static void main(String[] args) {
        Ford ford = new Ford();
        ford.fuel();
        ford.move();
        ford.start();
        ford.stop();

        ford.wheels();
        ford.key();


        A a = new A();
        a.testA();
        a.testX();

        B b = new B();
        b.testA();
        b.testB();
        b.testX();


        C c = new C();
        c.testC();
        c.testA();
        c.testX();

        // java doesn't allow multiple inheritance, but allows multi level of inheritance


    }
}
