package fifthpackage.oop;

public interface Vehicle { //extends  Vehicle2,Vehicle3{

    // interface = ideas

    // interface doesn't have a constructor that's why can't create obj
    // interface can be extended with other INTERFACE'S'
    // class can't have multiple parent but interface can have
    // In interface all the methods are public by default and can't have body

    void move();

    void start();

    void stop();

}
