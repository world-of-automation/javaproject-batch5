package fifthpackage.oop;

public class C extends A {

    // A parent can have 2 child

    public void testC() {
        System.out.println("Test C");
    }

}
