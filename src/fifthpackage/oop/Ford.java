package fifthpackage.oop;

public class Ford extends Car implements Vehicle {

    // Inheritance : extend
    // Abstraction : implement


    // i - i
    // interface --> implements
    // abstract class --> extends
    // first you extend, then you implement
    // a class can only have one extends (one parent)
    // and multiple implements


    // method overriding : same method name, different class, same params

    //Compile time polymorphism/ static binding/ static polymorphism = method overloading
    // Runtime polymorphism/ dynamic binding/ dynamic polymorphism = method overriding


    @Override
    public void fuel() {
        System.out.println("Ford's fuel is Diesel");
    }

    @Override
    public void move() {
        System.out.println("Ford can move");
    }

    @Override
    public void start() {
        System.out.println("Ford can start");
    }

    @Override
    public void stop() {
        System.out.println("Ford can stop");
    }
}
