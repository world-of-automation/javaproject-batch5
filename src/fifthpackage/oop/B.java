package fifthpackage.oop;

public class B extends A {

    // a class can't have 2 parents

    public void testB() {
        System.out.println("Test B");
    }
}
