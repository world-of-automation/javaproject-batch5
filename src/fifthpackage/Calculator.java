package fifthpackage;

public class Calculator {

    // Polymorphism : having many form

    // method overloading : same method name, same class, different params

    // method overriding : see package oop

    public static void addition(int a, int b) {
        System.out.println(a + b);
    }

    public static void addition(int a, int b, int c) {
        System.out.println(a + b + c);
    }

    public static void main(String[] args) {
        addition(10, 12);
        addition(10, 12, 11);
    }
}
