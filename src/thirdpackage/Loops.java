package thirdpackage;

public class Loops {

    public static void main(String[] args) {

        // for(startingPoint;endPoint;increment/decrement){--body starts
        // body ends--}

        for (int i = 0; i <= 5; i++) {
            //System.out.println("Java"+i);
        }

        for (int i = 10; i > 0; i--) {
            // System.out.println("Java"+i);
        }


        for (int i = 0; i < 10; i++) {
            System.out.println("Java");


            for (int j = 0; j < 3; j++) {
                System.out.println("Sub");
            }

        }


    }


}
