package thirdpackage;

public class SoccerTeam {

    static String nameOfTheClub;
    private final String manager;
    private final int totalPlayers;
    private String nameOfTrainer;


    public SoccerTeam(String manager, int totalPlayers) {
        this.manager = manager;
        this.totalPlayers = totalPlayers;
    }

    public static String getNameOfTheClub() {
        return nameOfTheClub + " is the name of the club";
    }

    public void league1() {
        System.out.println(manager + " is managing the team this season");
        System.out.println(totalPlayers + " is the number of the players");
    }

    public String getNameOfTheTrainer() {
        return nameOfTrainer + " is the trainer of the club";
    }

    public void setNameOfTrainer(String name) {
        this.nameOfTrainer = name;
    }

}
