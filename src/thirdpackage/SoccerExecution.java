package thirdpackage;

public class SoccerExecution {

    public static void main(String[] args) {
        // setup the required so that league1() prints the details

        SoccerTeam soccerTeam = new SoccerTeam("xyz", 11);
        soccerTeam.league1();


        // setup vthe required and get the value of getNameOfTheClub and print

        SoccerTeam.nameOfTheClub = "FCB";
        String name = SoccerTeam.getNameOfTheClub();
        System.out.println(name);


        soccerTeam.setNameOfTrainer("Something");
        String nameOfTrainerOfSoccerTeam = soccerTeam.getNameOfTheTrainer();
        System.out.println(nameOfTrainerOfSoccerTeam);


    }
}
