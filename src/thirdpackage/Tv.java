package thirdpackage;

public class Tv {

    //  static void
    public static void playSongs() {
        System.out.println("Tv plays songs");
    }

    // static return
    public static String getVideo() {
        return "video";
    }

    //  static void
    public static void playSongs(int numberOfSongs) {
        System.out.println("Tv plays " + numberOfSongs + " songs");
    }

    // static return
    public static String getVideo(String movieName) {
        String output = movieName + " is being played now";
        return output;
    }


    //****************************

    // non static void
    public void showMovies() {
        System.out.println("Tv shows movies");
    }

    // non static return
    public String getAudio() {
        return "sound";
    }

    // non static void
    public void showSpecificNumberOfMovie(int numberOfMovie) {
        System.out.println("Tv shows " + numberOfMovie + " movies");
    }

    // non static return
    public String getSpecificAudio(String songName) {
        String output = songName + " is being played now";
        return output;
    }


}
