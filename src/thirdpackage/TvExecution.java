package thirdpackage;

public class TvExecution {


    public static void main(String[] args) {

        Tv tv = new Tv();

        // non static void
        System.out.print("non static void prints : ");
        tv.showMovies();

        //  static void
        System.out.print("static void prints : ");
        Tv.playSongs();

        // non static return
        System.out.print("non static return prints : ");
        String audio = tv.getAudio();
        System.out.println(audio);


        // static return
        System.out.print("static return prints : ");
        String staticAudio = Tv.getVideo();
        System.out.println(staticAudio);


        //****************************

        // non static void
        System.out.print("non static void prints : ");
        tv.showSpecificNumberOfMovie(2);


        //  static void
        System.out.print("static void prints : ");
        Tv.playSongs(5);


        // non static return
        System.out.print("non static return prints : ");
        String songFromReturn = tv.getSpecificAudio("Whatever");
        System.out.println(songFromReturn);

        // static return
        System.out.print("static return prints : ");
        String movieFromReturn = Tv.getVideo("Something");
        System.out.println(movieFromReturn);

    }

}

