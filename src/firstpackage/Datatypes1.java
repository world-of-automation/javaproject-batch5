package firstpackage;

public class Datatypes1 {

    public static void main(String[] args) {
        // DataTypes variables = data ;
        String myName = "Zann";
        int zipcode = 11374;
        double salary = 45.5;
        boolean isItGonnaRainToday = true;

        System.out.println(myName + " " + zipcode);
        System.out.println(salary);
        System.out.println(isItGonnaRainToday);

        int x = 10;
        int y = 12;
        int z = 15;

        // addition of x & y
        // multiplication of y & z

        System.out.println(x + y);
        System.out.println(y * z);


        // do the addition of x & y and multiplication of y & z
        // and finally from the result of multiplication, subtract the result of addition
        // divide the final result by 10

        int resultOfAddition = x + y;
        int resultOfMultiplication = y * z;
        int finalResult = resultOfMultiplication - resultOfAddition;

        double totalResult = finalResult / 10.0;
        System.out.println(finalResult);
        System.out.println(totalResult);
    }
}
