package firstpackage;

public class PersonalDetails {

    // psvm --> main method
    // sout --> printline

    // global variables --> can access every method
    public static String name = "Zann";

    // main room
    public static void main(String[] args) {
        // print your name and your zip code in the same line
        System.out.println("Zann " + 11374);
        myJobInformation();
        myStudyInformation();
    }

    // room1
    public static void myStudyInformation() {
        // local variables --> can access only belonging method
        String college = "Queens College";
        System.out.println(name + " " + college);
    }

    // room2
    public static void myJobInformation() {
        String company = "NBCUniversal";
        System.out.println(name + " " + company);
    }
}
