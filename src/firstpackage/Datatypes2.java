package firstpackage;

public class Datatypes2 {

    // package name --> all lower case (no underscore,space,hyphen)
    // class name -->  Starts with upper case , and following word with uppercase (no underscore,space,hyphen)
    // method name : to do
    // variables name --> camel case (no space,hyphen), underscore can be used based on scenario --> to do

    public static void main(String[] args) {
        Integer x = 10;
        String country = "USA";
        String state = "NY";
        String county = "Queens";
        System.out.println(country + "," + state + "," + county);

        // boolean --> true/ false
        boolean isItGonnaRainToday = false;
        System.out.println(isItGonnaRainToday);

        // byte - 8 bit
        byte number2 = -128;
        byte number3 = 127;

        // short - 16 bit
        short number4 = -32768;
        short number5 = 32767;

        // int - 32 bit
        int number6 = -2_147_483_648;
        int number7 = 2_147_483_647;
        System.out.println(number6);

        // long - 64 bit
        long number88 = 34765948789l;

        // double - 64bit
        double number9 = 1.33d;

        // float - 32bit
        float number10 = 1.33f;

        char middleInitial = '!';

        //https://en.wikipedia.org/wiki/List_of_Unicode_characters
        char unicodeBeta = '\u016F';
        System.out.println(unicodeBeta);

    }
}
